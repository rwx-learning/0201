FROM python:3-alpine

WORKDIR /app

COPY requirements.txt /app/requirements.txt
RUN mkdir db && \
    pip3 install -r requirements.txt --no-cache-dir
COPY . /app

EXPOSE 8000
VOLUME ["/app/db"]

CMD sh ./init.sh && python3 manage.py runserver 0.0.0.0:8000
